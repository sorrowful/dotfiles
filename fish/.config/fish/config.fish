alias vi=nvim

fish_vi_key_bindings

# Base16 Shell
  if status --is-interactive
    eval sh /Users/robert/.base16-manager/chriskempson/base16-shell/scripts/base16-github.sh
  end

function quake2
    set -x start_dir (pwd)
    cd /Volumes/system-u/games/quake2
    ./q2pro -AppleMagnifiedMode YES +game action
    cd $start_dir
end

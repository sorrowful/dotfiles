" install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

" python
let g:python3_host_prog = '/usr/bin/python3'

" neoclide/coc for code completion
Plug 'jceb/vim-orgmode'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'cloudhead/neovim-fuzzy'
Plug 'Soares/base16.nvim'
"Plug 'leafOfTree/vim-svelte-plugin'
call plug#end()
 
" nvim theme 
au ColorScheme * hi Normal ctermbg=none guibg=none
source ~/.config/nvim/colorscheme.vim
hi Visual cterm=reverse ctermbg=none
set termguicolors

" airline theme
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='base16'

" fuzzy stuff
nnoremap <C-p> :FuzzyOpen<CR>

" vi stuff
set mouse=a
set tabstop=8
set expandtab
set shiftwidth=4
set autoindent
set smartindent
set cindent
set viminfo=
set number

" coc config (pretty much defaults)
source ~/.config/nvim/coc.vim
